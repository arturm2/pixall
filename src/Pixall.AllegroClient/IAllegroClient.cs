﻿using Pixall.AllegroClient.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Pixall.AllegroClient
{
    public interface IAllegroClient
    {
        Task<IList<Category>> GetCategories(string parentCategoryId);
    }
}
