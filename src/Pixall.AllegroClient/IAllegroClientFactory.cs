﻿namespace Pixall.AllegroClient
{
    public interface IAllegroClientFactory
    {
        IAllegroClient Create(string host, string clientId, string clientSecret);
    }
}
