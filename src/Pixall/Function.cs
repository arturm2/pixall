using Amazon.Lambda.APIGatewayEvents;
using Amazon.Lambda.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Pixall.AllegroClient;
using Pixall.AllegroClient.Model;
using Pixall.AllegroClientAdapter;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text.Json;


// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace Pixall
{

    public class Functions
    {

        IConfigurationService ConfigService { get; }
        IAllegroClientFactory AllegroClientFactory { get;  }

        /// <summary>
        /// Default constructor that Lambda will invoke.
        /// </summary>
        public Functions()
        {
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);
            var serviceProvider = serviceCollection.BuildServiceProvider();

            // Get Configuration Service from DI system
            ConfigService = serviceProvider.GetService<IConfigurationService>();
            AllegroClientFactory = serviceProvider.GetService<IAllegroClientFactory>();
        }

        private void ConfigureServices(IServiceCollection serviceCollection)
        {
            // Register services with DI system
            serviceCollection.AddTransient<IConfigurationService, ConfigurationService>();
            serviceCollection.AddTransient<IAllegroClientFactory, AllegroClientFactory>();
        }

        private IAllegroClient CreateClient()
        {
            var config = ConfigService.GetConfiguration();
            var host = config.GetValue<string>(Constants.AllegroHost);
            var clientId = config.GetValue<string>(Constants.AllegroClientId);
            var clientSecret = config.GetValue<string>(Constants.AllegroClientSecret);

            return AllegroClientFactory.Create(host, clientId, clientSecret);
        }

        public APIGatewayProxyResponse Categories(APIGatewayProxyRequest request, ILambdaContext context)
        {
            HttpStatusCode statusCode = default;
            string body = null;
            var headers = new Dictionary<string, string>();

            var categoryId = request.QueryStringParameters["categoryId"];

            var client = CreateClient();

            IList<Category> categories = null;
            try
            {
                categories = client.GetCategories(categoryId).Result;
                statusCode = HttpStatusCode.OK;
                headers.Add("Content-type", "application/json");
                body = JsonSerializer.Serialize(categories);
            }
            catch (AggregateException e)
            {
                // todo: ApplicationException?
                statusCode = HttpStatusCode.InternalServerError;
                headers.Add("Content-type", "text/plain");
                body = e.Message;
            }

            return new APIGatewayProxyResponse
            {
                StatusCode = (int)statusCode,
                Body = body,
                Headers = headers
            };
        }

        /// <summary>
        /// A Lambda function to respond to HTTP Get methods from API Gateway
        /// </summary>
        /// <param name="request"></param>
        /// <returns>The API Gateway response.</returns>
        //public APIGatewayProxyResponse Get(APIGatewayProxyRequest request, ILambdaContext context)
        //{
        //    context.Logger.LogLine("Get Request\n");

        //    var response = new APIGatewayProxyResponse
        //    {
        //        StatusCode = (int)HttpStatusCode.OK,
        //        Body = "Hello AWS Serverless",
        //        Headers = new Dictionary<string, string> { { "Content-Type", "text/plain" } }
        //    };

        //    return response;
        //}
    }
}
