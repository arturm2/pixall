﻿
using Microsoft.Extensions.Configuration;
using System.IO;

namespace Pixall
{
    class ConfigurationService : IConfigurationService
    {
        public IConfiguration GetConfiguration()
        {
            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddEnvironmentVariables()
                .AddSystemsManager(Constants.ParameterStoreRoot)
                .Build();
        }
    }

}
