﻿using Microsoft.Extensions.Configuration;

namespace Pixall
{
    interface IConfigurationService
    {
        IConfiguration GetConfiguration();
    }
}
