﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pixall
{
    class Constants
    {
        /// <summary>
        /// Prefix of variables in parameter store
        /// </summary>
        public const string ParameterStoreRoot = "/pixall";

        // Allegro client environment and credentials
        public const string AllegroHost = "allegroHost";
        public const string AllegroClientId = "allegroClientId";
        public const string AllegroClientSecret = "allegroClientSecret";
    }
}
