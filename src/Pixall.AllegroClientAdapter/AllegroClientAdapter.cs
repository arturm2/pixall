﻿using Pixall.AllegroClient;
using Pixall.AllegroClient.Model;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pixall.AllegroClientAdapter
{
    class AllegroClientAdapter : IAllegroClient
    {

        public AllegroClientAdapter(Allec.AllegroClient allegroClient)
        {
            _allegroClient = allegroClient;
        }

        public async Task<IList<Category>> GetCategories(string parentCategoryId)
        {
            var cats = await _allegroClient.RetrieveCategories(parentCategoryId);
            return cats.Categories
                .Select(c => new Category { Id = c.Id, Name = c.Name, ParentCategoryId = c.Parent.Id })
                .ToList();
        }

        private readonly Allec.AllegroClient _allegroClient;
    }
}
