﻿using Allec;
using Pixall.AllegroClient;

namespace Pixall.AllegroClientAdapter
{
    public class AllegroClientFactory : IAllegroClientFactory
    {

        public AllegroClientFactory()
        {
        }

        public IAllegroClient Create(string host, string clientId, string clientSecret)
        {
            var connInfo = new ConnectionInfo(host, clientId, clientSecret);
            var client = new Allec.AllegroClient(connInfo);
            return new AllegroClientAdapter(client);
        }
    }
}
